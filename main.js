///Theory

//1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//Когда пытаемся отловить потенциальную ошибку и вывести после какой-то результат.


///Practice

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const rootElement = document.getElementById("root");
rootElement.innerHTML = `<ul id="books"></ul>`;
let booksElement = document.getElementById("books");
books.forEach((book,index) => {
    booksElement.innerHTML += `<li class="book" id="book-${index}"></li>`
    let bookElement = document.getElementById(`book-${index}`);
    if (index % 2 === 0) {
        bookElement.classList.add("book-gray");
    } else {
        bookElement.classList.add("book-blue");
    }
    try {
        let textError = false
        if (book.author) {
            bookElement.innerHTML += `<p>${book.author}</p>`;
        } else {
            textError = "This book doesn't have author"
        }
        if (book.name) {
            bookElement.innerHTML += `<p>${book.name}</p>`;
        } else {
            textError = "This book doesn't have name"
        }
        if (book.price) {
            bookElement.innerHTML += `<p>${book.price}</p>`;
        } else {
            textError = "This book doesn't have price"
        }
        if (typeof (textError) === "string") {
            throw new Error(textError)
        }
    }
    catch (err) {
        console.error(err);
    }
})


